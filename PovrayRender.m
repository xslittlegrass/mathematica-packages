BeginPackage["PovrayRender`"]

povrayRender::usage="
Call povray to render Graphics3D object then load the rendered image into Mathematica.

p: a Graphics3D object
povpath: the path point to povray executable.

Example:

povrayRender[SphericalPlot3D[1+2Cos[2\[Theta]],{\[Theta],0,Pi},{\[Phi],0,2Pi},Mesh\[Rule]None,PlotPoints\[Rule]100],\"/Applications/PovrayCommandLineMac/Povray37UnofficialMacCmd\"]

Note that if the rendered object has very low resolution so that each individual triangle is viable, you should try to change the resolution in the Grpahics3D object you input. For example, do something like PlotPoints->100.
";

povrayRenderToFile::usage="
Call povray to render Graphics3D object then write the rendered graphics into file.
povrayRenderToFile[p_Graphics3D,outputPath_,povpath_]:

p: a Graphics3D object
outputPath: the path to output the rendered image. Should has a .png extension, and should be absolute path.
povpath: the path point to povray executable.

Example:

povrayRenderToFile[SphericalPlot3D[1+2Cos[2\[Theta]],{\[Theta],0,Pi},{\[Phi],0,2Pi},Mesh\[Rule]None,PlotPoints\[Rule]100],\"/Users/xslittlegrass/Downloads/11.png\",\"/Applications/PovrayCommandLineMac/Povray37UnofficialMacCmd\"]

Note that if the rendered object has very low resolution so that each individual triangle is viable, you should try to change the resolution in the Grpahics3D object you input. For example, do something like PlotPoints->100.
";

convertToPovLathe::usage="
This function format a list of points into a povray ready lathe object.
Input: pointLs is a list of points in the form of {{x1,y1},{x2,y2}....}


Example:

convertToPovLathe[Table[{x,Sin[x]},{x,0,2Pi,Pi/10}]]

";

generatePovrayString::usage="
This function create the povray sense file content as a string. It will remove the default light and texture style generated from the defualt export Export[1.pov,p].
";

Begin["`Private`"]

generatePovrayString[p_Graphics3D]:=Module[{addStr,povStr,replaceStr1,replaceStr2,replaceStr3},
addStr="
global_settings {
  max_trace_level 5
  assumed_gamma 1.0
  radiosity {
    pretrace_start 0.08
    pretrace_end   0.01
    count 35
    nearest_count 5
    error_bound 1.8
    recursion_limit 2
    low_error_factor .5
    gray_threshold 0.0
    minimum_reuse 0.015
    brightness 1
    adc_bailout 0.01/2
  }
}

#default {
  texture {
    pigment {rgb 1}
    finish {
      ambient 0.0
      diffuse 0.6
      specular 0.6 roughness 0.001
      reflection { 0.0 1.0 fresnel on }
      conserve_energy
    }
  }
}


light_source { <50,-50,50> 1 }
background { rgb <0,.25,.5> }
";


replaceStr1="finish {ambient color rgb 1}";(*remove the default light*)
replaceStr2="finish {ambient rgb 1}";(* remove additional light*)
replaceStr3="pigment {color rgb <0., 0., 0.>}";(*remove the default color*)

If[ByteCount[p]<200,(*if the graphics is empty*)
povStr=addStr;,
povStr=StringJoin[addStr,StringReplace[StringDrop[#,Last@Flatten@StringPosition[#,"/*ViewPoint*/"]]&@ExportString[p,"pov"],{replaceStr1->"",replaceStr2->"",replaceStr3->""}]];
povStr
]
(* export povray file to string from Graphics3D object *)

povrayExec[inputPath_,outputPath_,povpath_,povrayOptions_]:=Module[{renderCmd},
renderCmd=povpath<>" "<>povrayOptions<>""<>outputPath<>" "<>inputPath;
Run[renderCmd]
]
(* call povray exectuable to render the graphics *)

povrayRenderToFile[p_Graphics3D,outputPath_,povpath_]:=Module[{inputPath,povrayOptions,tempFileStream},
(*-----path for povray executables and options -------------*)
povrayOptions="+H1024 +W1280 +A0.001 -J +O";

(*----generate temporary pov file from Grpahics3D object----*)
(* this is intent to generate uniqu filename each time *)
inputPath=$TemporaryDirectory<>"/"<>DateString[{ToString@$KernelID,"Year","-","MonthNameShort","Day","-","Hour24","Minute","SecondExact"}]<>StringJoin@@ToString/@RandomInteger[{1,1000},{10}]<>".pov";
tempFileStream=OpenWrite[inputPath];
WriteString[inputPath,generatePovrayString[p]];
Close[tempFileStream];
(*------call povray to render and output rendered file------*)
povrayExec[inputPath,outputPath,povpath,povrayOptions];
outputPath
]

(* put together the subroutines, output the rendered image to file *)

povrayRender[p_Graphics3D,povpath_]:=Module[{inputPath,outputPath,povrayOptions,tempFileStream},
(*-----path for povray executables and options -------------*)
povrayOptions="+H1024 +W1280 +A0.001 -J +O";

(*----generate temporary pov file from Grpahics3D object----*)
(* this is intent to generate uniqu filename each time *)
inputPath=$TemporaryDirectory<>"/"<>DateString[{ToString@$KernelID,"Year","-","MonthNameShort","Day","-","Hour24","Minute","SecondExact"}]<>StringJoin@@ToString/@RandomInteger[{1,1000},{10}]<>".pov";
tempFileStream=OpenWrite[inputPath];
WriteString[inputPath,generatePovrayString[p]];
Close[tempFileStream];

outputPath=$TemporaryDirectory<>"/"<>DateString[{ToString@$KernelID,"Year","-","MonthNameShort","Day","-","Hour24","Minute","SecondExact"}]<>StringJoin@@ToString/@RandomInteger[{1,1000},{10}]<>".png";

(*------call povray to render and output rendered file------*)
povrayExec[inputPath,outputPath,povpath,povrayOptions];
Import[outputPath]
]

convertToPovLathe[pointLs_]:=Module[{ptFormat,sorPovHead,sorPovTail,sorPovBody},

ptFormat[{x_,y_}]:=StringJoin[{"<",ToString@NumberForm[If[Abs@x<1.*^-5,0,x],{8,7}],",",ToString@NumberForm[If[Abs@y<1.*^-5,0,y],{8,7}],">"}];
sorPovHead="\n\nlathe{\n cubic_spline \n"<>ToString[Length[pointLs]]<>",\n";
sorPovTail="\n sturm \n
	texture{
       pigment{color White}
       finish { phong 0.5}}
  }\n\n";
sorPovBody=StringJoin[Riffle[ptFormat/@pointLs,",\n"]];
StringJoin[{sorPovHead,sorPovBody,sorPovTail}]
]

End[]

EndPackage[]